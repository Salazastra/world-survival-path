﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float speed = 3f;
    Rigidbody rb;
    Animator anim;
    public static Player player;
    bool isRight = true;
    float muve;

    Transform cam;
    Vector3 deltaCamPos;
    Vector3 newPosCam;
    float posX, posY;

    bool isCor;

    void Awake()
    {
        player = this;
    }

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform.parent;
    }

    void Update()
    {
        cam.position = transform.position;
    }

    void FixedUpdate()
    {
        if (!isCor)
        {
            muve = Input.GetAxis("Horizontal");
            rb.velocity = transform.TransformDirection(0, rb.velocity.y, muve * speed * (isRight ? 1 : -1));
            if ((isRight && muve < -0.3f)||(!isRight && muve > 0.3f))
            {
                StartCoroutine(Povorot180());
                isRight = !isRight;
            }
        }


        //transform.Rotate(0, Input.GetAxis("Mouse X"), 0, Space.World);
        //anim.SetFloat("speed", rb.velocity.z);
    }

    void OnTriggerEnter(Collider coll)
    {

    }

    IEnumerator Povorot180()
    {
        isCor = true;
        for (int i = 0; i < 30; i++)
        {
            yield return null;
            transform.Rotate(0, 6, 0);
        }
        isCor = false;
    }

    public IEnumerator Povorot(int rot)
    {
        isCor = true;
        yield return new WaitForSeconds(0.1f);
        if(rot == 90)
        {
            cam.Rotate(0, 90, 0);
            for (int i = 0; i < 30; i++)
            {
                rb.velocity = transform.TransformDirection(0, 0, speed * 4);
                yield return new WaitForFixedUpdate();
                transform.Rotate(0, 3, 0);
            }
           
        }
        else if(rot == -90)
        {
            cam.Rotate(0, -90, 0);
            for (int i = 0; i < 30; i++)
            {
                rb.velocity = transform.TransformDirection(0, 0, speed * 4);
                yield return new WaitForFixedUpdate();
                transform.Rotate(0, -3, 0);
            }
           
        }
        else if (rot == 180)
        {
            cam.Rotate(0, 180, 0);
            for (int i = 0; i < 60; i++)
            {
                rb.velocity = transform.TransformDirection(0, 0, speed * 3);
                yield return new WaitForFixedUpdate();
                transform.Rotate(0, 3, 0);
            }

        }
        else if (rot == -180)
        {
            cam.Rotate(0, -180, 0);
            for (int i = 0; i < 60; i++)
            {
                rb.velocity = transform.TransformDirection(0, 0, speed * 3);
                yield return new WaitForFixedUpdate();
                transform.Rotate(0, -3, 0);
            }

        }

        isCor = false;
    }

}
