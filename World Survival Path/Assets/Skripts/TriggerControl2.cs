﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TriggerControl2 : MonoBehaviour
{
    public bool isRight;
    public int notAnge = 90;
    public Transform left;
    public Transform right;
    public LayerMask player;


    void OnTriggerEnter(Collider coll)
    {
        if (coll.tag == "player")
        {
            if (isRight && Physics.OverlapBox(right.position, new Vector3(0.5f, 0.5f, 4f), Quaternion.identity, player).Length > 0)
            {
                StartCoroutine(coll.GetComponent<Player1>().Povorot(notAnge));
            }
            else if (!isRight && Physics.OverlapBox(left.position, new Vector3(0.5f, 0.5f, 4f), Quaternion.identity, player).Length > 0)
            {
                StartCoroutine(coll.GetComponent<Player1>().Povorot(notAnge));
            }
        }
    }
}
