﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player1 : MonoBehaviour
{
    public float speed = 3f;
    float move;
    Rigidbody rb;
    Transform cam;
    Animator anim;
    bool isRight = true;
    bool isCor;
    bool isJump;

    void Start()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform.parent;
    }

    void Update()
    {
        move = Input.GetAxis("Vertical");
        anim.SetFloat("speed", Mathf.Abs(move));
        if (!isCor)
        {
            move = Input.GetAxis("Vertical");
            rb.velocity = transform.TransformDirection(0, rb.velocity.y, move * speed * (isRight ? 1 : -1));
            transform.Rotate(0, Input.GetAxis("Mouse X"), 0, Space.World);
            cam.position = transform.position;
            
            if ((isRight && move < -0.3f) || (!isRight && move > 0.3f))
            {
                StartCoroutine(Povorot180());
                isRight = !isRight;
            }
        }
        if (Input.GetKeyDown(KeyCode.C))
        {
            Cursor.visible = !Cursor.visible;
            Cursor.lockState = Cursor.visible ? CursorLockMode.None : CursorLockMode.Locked;
        }
        if (isJump && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(0, 600, 800);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Jump")
        {
            isJump = true;
            print("press Spase to Jump");
        }

    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag == "Jump")
        {
            isJump = false;
        }
    }
    IEnumerator Povorot180()
    {
        //cam.Rotate(0, 180, 0);
        for (int i = 0; i < 30; i++)
        {
            yield return null;
            transform.Rotate(0, 6, 0);
        }
    }
    public IEnumerator Povorot(int rot)
    {
        yield return new WaitForSeconds(0.1f);
        if (rot == 90)
        {
            cam.Rotate(0, 90, 0);
        }
        else if (rot == -90)
        {
            cam.Rotate(0, -90, 0);
        }
        else if (rot == 180)
        {
            cam.Rotate(0, 180, 0);
        }
        else if (rot == -180)
        {
            cam.Rotate(0, -180, 0);
        }
    }
}
